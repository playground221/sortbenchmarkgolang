package main


//var debug bool


func debugSlice(s []int) string {
//  ms:=myslice(s)
//  return fmt.Sprintf("ptr=%p len=%d cap=%d",ms.ptr, ms.len, ms.cap)
return ""
  
}

/*
func getGID() uint64 {
    b := make([]byte, 64)
    b = b[:runtime.Stack(b, false)]
    b = bytes.TrimPrefix(b, []byte("goroutine "))
    b = b[:bytes.IndexByte(b, ' ')]
    n, _ := strconv.ParseUint(string(b), 10, 64)
    return n
}
*/

func merge(src []int, dst []int, left,middle,right int) {
  lslice:=src[left:middle]
  rslice:=src[middle:right]
  dslice:=dst[left:right]
/*  if( debug) {
    fmt.Println("func merge premerge ", src, " ", dst, left,middle,right, " lslice ",lslice, "rslice",rslice)
  }*/

  for ; len(lslice) > 0 && len (rslice) >0; {
    if( lslice[0] < rslice[0]) {
      dslice[0]=lslice[0]
      dslice=dslice[1:]
      lslice=lslice[1:]
//      fmt.Println("func merge postmerge ", src, " ", dst, left,middle,right, " lslice ",lslice, "rslice",rslice)
    } else {
      dslice[0]=rslice[0]
      dslice=dslice[1:]
      rslice=rslice[1:]
//      fmt.Println("func merge postmerge ", src, " ", dst, left,middle,right, " lslice ",lslice, "rslice",rslice)
    }
  }
  if( len(lslice) ==0 && len(rslice) == 0 ){
    return
  }
  if( len(lslice) == 0){
    copy(dslice,rslice)
//  fmt.Println("func merge postmerge ", src, " ", dst, left,middle,right, " lslice ",lslice, "rslice",rslice)
  }
  if( len(rslice) == 0){
    copy(dslice,lslice)
//    fmt.Println("func merge postmerge ", src, " ", dst, left,middle,right, " lslice ",lslice, "rslice",rslice)
  }
}

/*
func debug_slice_and_splice(t string, s,d []int, i int){
  fmt.Print(t," ", "Goroutine id ", getGID(),"; source=",s,"; dst=",d,"; iterationstiefe=",i,"\n")
}
*/

func slice_and_splice(origsrc, origdst []int, iterationstiefe int, done chan bool){
//  debug_slice_and_splice("vor Sortierung",origsrc,origdst,iterationstiefe)
  src:=origsrc
  dst:=origdst
//  fmt.Printf("debug goid %d pre-swap src-addr %p dst-addr %p\n",getGID(), &src, &dst)
  if( iterationstiefe % 2 == 1) {
//    fmt.Println("debug goid ",getGID(), " slice_and_splice switching src ", src, " and dst ", dst)
    src,dst=dst,src //Verdacht auf defekt
  }
//  fmt.Printf("debug goid %d post-swap src-addr %p dst-addr %p\n",getGID(), &src, &dst)
  l:=len(src)
  if(l>2){
    left:=0
    middle:=l/2
    right:=l
    ldone:=make( chan bool)
    rdone:=make( chan bool)
    go slice_and_splice(origsrc[left:middle], origdst[left:middle],iterationstiefe+1, ldone)
    go slice_and_splice(origsrc[middle:right],origdst[middle:right],iterationstiefe+1, rdone)
    <-ldone
    <-rdone
    merge(src,dst,left,middle,right)
//    debug_slice_and_splice("nach Sortierung ", src,dst,iterationstiefe)
    done<-true
    return
  }
  if( l==2){
    merge(src,dst,0,1,2)
//    debug_slice_and_splice("nach Sortierung", src,dst,iterationstiefe)
    done<-true
    return
  }
  if( l==1){
    origdst[0]=origsrc[0]
//    debug_slice_and_splice("nach Sortierung", src,dst,iterationstiefe)
    done<-true
    return
  }
//  debug_slice_and_splice("nach Sortierung", src,dst,iterationstiefe)
  done<-true
  return
}

func prepare(unsorted []int) []int {
  sorted:=make([]int,len(unsorted));
  copy( sorted, unsorted)
  return sorted
}

func mergesort_multithread(unsorted []int) []int {
  sorted := prepare(unsorted)
  var iterationstiefe int
  iterationstiefe=0;
  done:=make(chan bool)
  go slice_and_splice(unsorted,sorted,iterationstiefe,done)
  <-done
  return sorted
}

