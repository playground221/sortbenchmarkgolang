package main

import "reflect"
import "testing"
import "fmt"

func TestMerge1(t *testing.T){
  a:=[]int{1,3}
  b:=[]int{2}
  s:=append(a, b...)
  d:=make([]int,3)
  r:=[]int{1,2,3}
  merge(s,d,0,2,3)
  if !reflect.DeepEqual(r,d) {
    t.Fatalf("Merge fehlgeschlagen")
  }
}
func TestMerge2(t *testing.T){
  a:=[]int{1,3}
  b:=[]int{}
  r:=[]int{1,3}
  s:=append(a,b...)
  d:=make([]int,2)
  merge(s,d,0,2,2)
  if !reflect.DeepEqual(d,r) {
    t.Fatalf("Merge mit leerer Liste fehlgeschlagen")
  }
}

func TestPreprae1(t *testing.T){
  a:=[]int{1,2,3}
  b:=prepare(a)
  if !reflect.DeepEqual(a,b) {
    t.Fatalf("Arraycopy fehlgeschlagen")
  }
}

func TestMergeTwoElem(t *testing.T){
  src:=[]int{9,3}
  dst:=[]int{0,0}
  exp:=[]int{3,9}
  merge(src,dst,0,1,2)
  if !reflect.DeepEqual(exp,dst) {
    t.Fatalf("merge zwei Elemente fehlgeschlagen")
  }
}

func srtcmp(t *testing.T,u,s[]int){
  if !reflect.DeepEqual(s,mergesort_multithread(u)) {
    errmsg:=fmt.Sprintf("%s %v %s %v %s %v", "mergesort_multithread ",len(u)," Elemente fehlgeschlagen, expected ",s, " got ",mergesort_multithread(u))
    t.Fatalf(errmsg)
  }
}
func TestSize1(t *testing.T){
  u:=[]int{9,8}
  s:=[]int{8,9}
  srtcmp(t,u,s)
}
  
func TestSize2(t *testing.T){
  u:=[]int{9,8,7}
  s:=[]int{7,8,9}
  srtcmp(t,u,s)
}
func TestSize3(t *testing.T){
  u:=[]int{3,1,1}
  s:=[]int{1,1,3}
  srtcmp(t,u,s)
}
func TestSize4(t *testing.T){
  u:=[]int{1,3,1}
  s:=[]int{1,1,3}
  srtcmp(t,u,s)
}
func TestSize5(t *testing.T){
  u:=[]int{1,1,3}
  s:=[]int{1,1,3}
  srtcmp(t,u,s)
}  
func TestSize6(t *testing.T){
  u:=[]int{4,1,1,1}
  s:=[]int{1,1,1,4}
  srtcmp(t,u,s)
}
func TestSize7(t *testing.T){
  u:=[]int{1,4,1,1}
  s:=[]int{1,1,1,4}
  srtcmp(t,u,s)
}  
func TestSize8(t *testing.T){
  u:=[]int{1,1,4,1}
  s:=[]int{1,1,1,4}
  srtcmp(t,u,s)
}
func TestSize9(t *testing.T){
  u:=[]int{1,1,1,4}
  s:=[]int{1,1,1,4}
  srtcmp(t,u,s)
}
func TestSize10(t *testing.T){
  u:=[]int{5,1,1,1,1}
  s:=[]int{1,1,1,1,5}
  srtcmp(t,u,s)
}
func TestSize11(t *testing.T){
  u:=[]int{1,5,1,1,1}
  s:=[]int{1,1,1,1,5}
  srtcmp(t,u,s)
}
func TestSize14(t *testing.T){
  u:=[]int{1,1,5,1,1}
  s:=[]int{1,1,1,1,5}
  srtcmp(t,u,s)
}
func TestSize12(t *testing.T){
//  debug=true
  u:=[]int{1,1,1,5,1}
  s:=[]int{1,1,1,1,5}
  srtcmp(t,u,s)
}
func TestSize13(t *testing.T){
  u:=[]int{1,1,1,1,5}
  s:=[]int{1,1,1,1,5}
  srtcmp(t,u,s)
}