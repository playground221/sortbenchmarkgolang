package main

import "fmt"

type MathFunc func(int, int) int
var operatorList []MathFunc

func add(a,b int) int {
  return a+b
}
func sub(a,b int) int {
  return a-b
}

func main(){
  operatorList=append(operatorList,add)
  operatorList=append(operatorList,sub)
  for _,op:=range(operatorList){
    a:=5
    b:=3
    fmt.Println(a,b,op(a,b))
  }
}